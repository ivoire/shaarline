# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from datetime import datetime
import logging

from databases import Database
from sqlalchemy import (
    create_engine,
    Column,
    ForeignKey,
    Integer,
    String,
    Table,
    DateTime,
    Text,
    MetaData,
)


LOG = logging.getLogger("shaarline.db")

metadata = MetaData()

Shaarlis = Table(
    "shaarlis",
    metadata,
    Column("url", String, primary_key=True),
    Column("title", String),
)

Items = Table(
    "items",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("shaarli", String, ForeignKey("shaarlis.url", ondelete="CASCADE")),
    Column("link", String),
    Column("title", String),
    Column("pubdate", DateTime(timezone=True)),
    Column("description", Text),
)


@dataclass
class Item:
    id: int
    shaarli: str
    link: str
    title: str
    pubdate: datetime
    description: str


@dataclass
class Shaarli:
    url: str
    title: str
    items: int = 0


async def db_startup(app):
    # Create the databases using sqlalchemy magic
    engine = create_engine(app["options"].url)
    metadata.create_all(engine)
    engine.dispose()

    LOG.info("[INIT] Create the database")
    database = Database(app["options"].url)
    await database.connect()
    app["db"] = database


async def db_shutdown(app):
    LOG.info("[END] Close the database")
    await app["db"].disconnect()
