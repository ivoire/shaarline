# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

import aiohttp
from aiohttp import web
import aiohttp_jinja2
import argparse
import asyncio
import jinja2
import logging

from sqlalchemy import bindparam, desc

from .__about__ import __version__
from .db import Item, Items, Shaarli, Shaarlis, db_startup, db_shutdown
from .tasks import fetch


FORMAT = "%(asctime)-15s %(levelname)7s %(message)s"
LOG = logging.getLogger("shaarline")

# TODO: should be configurable
HANDLER = logging.StreamHandler()
HANDLER.setFormatter(logging.Formatter(FORMAT))
LOG.addHandler(HANDLER)
# TODO: should be configurable
LOG.setLevel(logging.DEBUG)


@aiohttp_jinja2.template("index.html")
async def handle_home(request):
    database = request.app["db"]
    rows = await database.fetch_all(Shaarlis.select())
    shaarlis = {url: title for (url, title) in rows}

    # Handle pagination
    PER_PAGE = 25
    items_count = await database.fetch_val(Items.count())
    page = int(request.match_info.get("page", 0))
    pages = int(items_count / PER_PAGE)

    rows = await database.fetch_all(
        Items.select().order_by(desc("pubdate")).offset(page * PER_PAGE).limit(PER_PAGE)
    )
    items = [Item(*row) for row in rows]
    return {
        "version": __version__,
        "shaarlis": shaarlis,
        "items": items,
        "pages": pages,
        "page": page,
    }


@aiohttp_jinja2.template("shaarlis.html")
async def handle_shaarlis(request):
    database = request.app["db"]
    query = Shaarlis.select().order_by("title")
    rows = await database.fetch_all(str(query))

    shaarlis = []
    for row in rows:
        query = Items.count().where(Items.c.shaarli == bindparam("shaarli"))
        count = await database.fetch_val(str(query), {"shaarli": row[0]})
        shaarlis.append(Shaarli(row[0], row[1], count))
    return {"version": __version__, "shaarlis": shaarlis}


async def on_startup(app):
    await db_startup(app)
    app["task"] = asyncio.create_task(fetch(app))


async def on_shutdown(app):
    app["task"].cancel()
    await app["task"]
    await db_shutdown(app)


def main():
    # Parse the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default="localhost")
    parser.add_argument("--port", default=8001)
    parser.add_argument("--url", default="sqlite:///db.sqlite3")
    options = parser.parse_args()

    # Create the aiohttp application
    app = web.Application()
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("shaarline/templates"))

    # Variables
    app["options"] = options
    app["static_root_url"] = "/static"

    # Routes
    app.add_routes(
        [
            web.get("/", handle_home),
            web.get("/{page:\\d+}/", handle_home),
            web.get("/shaarlis/", handle_shaarlis),
            web.static("/static", "shaarline/static"),
        ]
    )

    # signals
    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)

    # Run the application
    LOG.info("[INIT] Listening on http://%s:%d", options.host, options.port)
    # TODO: print access logs
    web.run_app(app, host=options.host, port=options.port, print=False, access_log=LOG)
    return 0
