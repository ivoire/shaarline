# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

import sys

from . import main


if __name__ == "__main__":
    sys.argv[0] = "shaarline"
    sys.exit(main())
