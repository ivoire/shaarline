# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

import aiohttp
import asyncio
from dateutil import parser
import logging
from sqlalchemy import bindparam

import feedparser

from .__about__ import __version__
from .db import Items, Shaarli, Shaarlis


LOG = logging.getLogger("shaarline.tasks")
HEADERS = {"User-Agent": f"shaarline v{__version__}"}


async def fetch_one(db, http, shaarli):
    LOG.debug("Fetching %r", shaarli.url)
    # TODO: add timeout
    try:
        resp = await http.get(shaarli.url, params={"do": "rss"}, timeout=5)
        if resp.status != 200:
            LOG.warning("Error %d when fetching %r", resp.status_code, shaarli.url)
            return

        # Parse the feed
        text = await resp.text()
        feed = feedparser.parse(text)

        # Update the shaarli
        shaarli.title = feed.get("feed").get("title")
        query = (
            Shaarlis.update()
            .where(Shaarlis.c.url == bindparam("url"))
            .values(title=bindparam("title"))
        )
        await db.execute(str(query), {"url": shaarli.url, "title": shaarli.title})

        # Add the entries
        for entry in feed.get("entries"):
            link = entry.get("link")
            title = entry.get("title")
            description = entry.get("description")
            pubdate = parser.parse(entry.get("published"))
            # TODO: handle tags

            query = (
                Items.select()
                .where(Items.c.shaarli == bindparam("shaarli"))
                .where(Items.c.link == bindparam("link"))
            )
            row = await db.fetch_one(str(query), {"shaarli": shaarli.url, "link": link})
            if row is None:
                await db.execute(
                    Items.insert(),
                    {
                        "shaarli": shaarli.url,
                        "link": link,
                        "title": title,
                        "description": description,
                        "pubdate": pubdate,
                    },
                )
            else:
                query = (
                    Items.update()
                    .where(Items.c.shaarli == bindparam("shaarli"))
                    .where(Items.c.link == bindparam("link"))
                    .values(
                        title=bindparam("title"),
                        description=bindparam("description"),
                        pubdate=bindparam("pubdate"),
                    )
                )
                await db.execute(
                    str(query),
                    {
                        "shaarli": shaarli.url,
                        "link": link,
                        "title": title,
                        "description": description,
                        "pubdate": pubdate,
                    },
                )
    except aiohttp.ClientError as exc:
        LOG.error("Unable to fetch %r: %s", shaarli.url, exc)
    except asyncio.TimeoutError:
        LOG.error("Unable to fetch %r: timeout", shaarli.url)
    except Exception as exc:
        LOG.exception(exc)


async def fetch(app):
    LOG.info("[INIT] Starting task 'fetch'")
    database = app["db"]
    async with aiohttp.ClientSession(headers=HEADERS) as http:
        while True:
            # TODO: error handling?
            rows = await database.fetch_all(Shaarlis.select())
            shaarlis = [Shaarli(*row) for row in rows]
            futures = [fetch_one(database, http, s) for s in shaarlis]
            await asyncio.gather(*futures)
            # TODO: should be a variable
            await asyncio.sleep(600)
