# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

__version__ = "0.1"
