#!/bin/sh
#
# Copyright 2019 Linaro Limited
#
# Author: Rémi Duraffort <ross@duraffort.fr>
#
# SPDX-License-Identifier: MIT

set -e

[ -n "$1" ] && exec "$@"

exec python3 -m shaarline --host "*"
