FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt-get update -q && \
    apt-get install --no-install-recommends --yes python3 python3-aiohttp python3-aiohttp-jinja2 python3-dateutil python3-feedparser python3-jinja2 python3-pip python3-sqlalchemy && \
    python3 -m pip install databases[sqlite] sentry-sdk && \
    # Cleanup
    apt-get clean && \
    find /usr/lib/python3/dist-packages/ -name '__pycache__' -type d -exec rm -r "{}" + && \
    rm -rf /var/lib/apt/lists/*

# Create the django project
WORKDIR /app/

# Add entrypoint
COPY share/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Add sources
COPY shaarline/ /app/shaarline/
